package me.paul2708.generator.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import me.paul2708.generator.frame.Generator;
import me.paul2708.generator.util.Util;

public class CopyActionListener implements ActionListener {
	
	public void actionPerformed(ActionEvent e) {
		if((e.getSource() != null) && (e.getSource() instanceof JButton)) {
			JButton button = (JButton) e.getSource();
			if(Generator.getInstance().copyButton.equals(button)) {
				String hash = Generator.getInstance().hashArea.getText();
				if((hash == null) || (hash.equals(""))) return;
				Util.copyToClipBorad(hash);
			}
		}
	}
}