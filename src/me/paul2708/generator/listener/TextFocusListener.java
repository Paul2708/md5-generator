package me.paul2708.generator.listener;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JTextArea;
import me.paul2708.generator.frame.Generator;

public class TextFocusListener implements FocusListener {
	
	public void focusGained(FocusEvent e) {
		if((e.getSource() != null) && (e.getSource() instanceof JTextArea)) {
			JTextArea area = (JTextArea) e.getSource();
			if(Generator.getInstance().textArea.equals(area)) {
				area.setForeground(Color.BLACK);
				area.setText("");
				Generator.getInstance().hashArea.setText("");
			}
		}
	}

	public void focusLost(FocusEvent e) {}
}