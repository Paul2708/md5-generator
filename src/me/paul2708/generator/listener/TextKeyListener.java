package me.paul2708.generator.listener;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import me.paul2708.generator.frame.Generator;

public class TextKeyListener implements KeyListener {
	
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == 10) {
			Generator.getInstance().encodeButton.doClick();
		}
	}

	public void keyReleased(KeyEvent e) {}

	public void keyTyped(KeyEvent e) {}
}