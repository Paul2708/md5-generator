package me.paul2708.generator.listener;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import me.paul2708.generator.frame.Generator;
import me.paul2708.generator.util.Util;

public class EncodeActionListener implements ActionListener {
	
	public void actionPerformed(ActionEvent e) {
		if((e.getSource() != null) && (e.getSource() instanceof JButton)) {
			JButton button = (JButton) e.getSource();
			if(Generator.getInstance().encodeButton.equals(button)) {
				String text = Generator.getInstance().textArea.getText();
				if(Generator.getInstance().textArea.getForeground() == Color.GRAY) return;
				if(Generator.getInstance().textArea.getText().equals("")) return;
				String result = Util.createMD5Hash(text);
				Generator.getInstance().hashArea.setText(result);
				Generator.getInstance().hashArea.requestFocusInWindow();
			}
		}
	}
}