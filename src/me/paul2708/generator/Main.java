package me.paul2708.generator;

import java.awt.EventQueue;

import me.paul2708.generator.frame.Generator;

public class Main {
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Generator frame = new Generator();
					frame.setVisible(true);
					frame.setState(0);
					frame.hashArea.requestFocusInWindow();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}