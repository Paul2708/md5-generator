package me.paul2708.generator.frame;

import java.awt.Color;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import me.paul2708.generator.listener.CopyActionListener;
import me.paul2708.generator.listener.EncodeActionListener;
import me.paul2708.generator.listener.TextFocusListener;
import me.paul2708.generator.listener.TextKeyListener;

public class Generator extends JFrame {

	private static final long serialVersionUID = 2819298319910419881L;
	private static Generator instance;
	
	public JLabel zeichenLabel;
	public JLabel hashLabel;
	public JTextArea textArea;
	public JTextArea hashArea;
	public JButton encodeButton;
	public JButton copyButton;

	public Generator() {
		instance = this;

		setTitle("MD5 - Generator");
		URL url = Generator.class.getResource("icon.png");
		ImageIcon icon = new ImageIcon(url);
		setIconImage(icon.getImage());
		setSize(450, 300);
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(3);

		JPanel panel = new JPanel();
		add(panel);
		placeComponents(panel);
	}

	private void placeComponents(JPanel panel) {
		panel.setLayout(null);

		this.zeichenLabel = new JLabel("Zeichenkette");
		this.zeichenLabel.setBounds(10, 11, 110, 14);
		panel.add(this.zeichenLabel);

		this.textArea = new JTextArea();
		this.textArea.setBounds(10, 36, 269, 60);
		this.textArea.setText("Gebe deinen Text ein...");
		this.textArea.setForeground(Color.GRAY);
		this.textArea.addKeyListener(new TextKeyListener());
		this.textArea.addFocusListener(new TextFocusListener());
		panel.add(this.textArea);

		this.hashLabel = new JLabel("MD5 - Hash");
		this.hashLabel.setBounds(10, 118, 90, 14);
		panel.add(this.hashLabel);

		this.hashArea = new JTextArea();
		this.hashArea.setColumns(10);
		this.hashArea.setEditable(false);
		this.hashArea.setBounds(10, 143, 269, 60);
		panel.add(this.hashArea);

		this.encodeButton = new JButton("Verschlüsseln");
		this.encodeButton.setBounds(318, 36, 116, 23);
		this.encodeButton.addActionListener(new EncodeActionListener());
		panel.add(this.encodeButton);

		this.copyButton = new JButton("Kopieren");
		this.copyButton.setBounds(318, 143, 116, 23);
		this.copyButton.addActionListener(new CopyActionListener());
		panel.add(this.copyButton);
	}

	/**
	 * Gibt die aktuelle instance vom Generator wieder.
	 * 
	 * @return Generator instance
	 */
	public static Generator getInstance() {
		return instance;
	}
}